#include <iostream>
#include <list>
#include <string>
#include "Graph.h"
using namespace std;
int minDistance(int dist[], bool sptSet[], int N);
int main(){
	list<int> ArrayList;
	Graph earn;
	int node;

    // dimensions
    cout<<"Enter a number of nodes: ";
    cin>>node;

    // dynamic allocation
    int row = node, col = node;
    int** ary = new int*[row];
    for(int i = 0; i < row; ++i){ary[i] = new int[col];}

    // fill
    cout<<"Enter a number of the path between each node: ";
    for(int i = 0; i < row; ++i){for(int j = 0; j < col; ++j)cin>>ary[i][j];}

    // print
    cout<<"\nAdjacent Matrix of this graph is: "<<endl;
    for(int i = 0; i < row; ++i)
        for(int j = 0; j < col; ++j){
            cout<<ary[i][j]<<" ";
            if(j == col-1){cout<<endl;}
        }

    // Send all of the value into the list
	for(int i = 0; i < row; i++){
		for(int j = 0; j < col; j++){
			ArrayList.push_back(ary[i][j]);
		}
	}

    // Call function in Graph.h
    // Send array list with its size
	earn.pushInList(node,ArrayList);

	cout<<"\nAdjacent List of this graph is: "<<endl;
	earn.showList(node);
	cout<<endl;
	if(earn.Multig(4))      {cout<<endl<<"This graph is MultiGraph";}
	if(earn.Pseudog(4))     {cout<<endl<<"This graph is PseudoGraph";}
    if(earn.Directg(4))     {cout<<endl<<"This graph is DirectGraph";}
	if(earn.Weightg(4))     {cout<<endl<<"This graph is WeightGraph";}
	if(earn.Completeg(4))   {cout<<endl<<"This graph is CompleteGraph";}
	cout<<endl;

	// Funtion that implements Dijkstra's single source shortest path algorithm
	int dist[node];
	int src = 0;
    bool sptSet[node];

    for (int i = 0; i < node; i++) dist[i] = INT_MAX, sptSet[i] = false;

    dist[src] = 0;
    for (int count = 0; count < node-1; count++){
        int x = minDistance(dist, sptSet, node);
        sptSet[x] = true;
        for (int y = 0; y < node; y++)
            if (!sptSet[y] && ary[x][y] && dist[x] != INT_MAX && dist[x]+ary[x][y] < dist[y]) dist[y] = dist[x] + ary[x][y];
    }

    //Print Dijkstra's Solution
    cout<<"\nDijkstra's Solution\nDistance from Source of each vertex:\n";
    for (int i = 0; i < node; i++) cout<<dist[i]<<endl;

	// clean up
	for(int i = 0; i < row; ++i) {delete [] ary[i];}
    delete [] ary;

	return 0;
}

//minDistance
int minDistance(int dist[], bool sptSet[], int N){
   int min = INT_MAX, min_index;
   for (int v = 0; v < N; v++)
     if (sptSet[v] == false && dist[v] <= min) min = dist[v], min_index = v;
   return min_index;
}



